class Training{
    /**
     * 
     * @param {String} name 
     * @param {int} id 
     * @param {String} detail 
     */
    constructor(name, id, detail){
        this.name=name
        this.id=id
        this.detail=detail
    }
}