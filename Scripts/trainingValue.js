class TrainingValue{
    /**
     * 
     * @param {int} trainingId 
     * @param {*} value 
     */
    constructor(trainingId,value){
        this.trainingId=trainingId
        this.value=value
    }
}